<?php
include_once '../../conexion/conectar.php';

$sel_pers = "SELECT * FROM personal ORDER BY nombre_personal ASC";
$eje_pers = mysqli_query($Cnn, $sel_pers);


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../styles/css/bootstrap.min.css">
</head>
<body>
   <div class="container">
      <span class="pull-rigth">
          <a href="agregar_personal.php" class="btn btn-success btn-xs">Nuevo</a>
      </span>
      <div style="height: 4px;"></div>
       <table class="table table-hover table-bordered">
           <thead>
               <tr>
                   <th>Id</th>
                   <th>Nombre Completo</th>
                   <th>Direccion</th>
                   <th>Telefono</th>
                   <th>Fecha Nac.</th>
                   <th>Sexo</th>
                   <th colspan="2">Acciones</th>
               </tr>
           </thead>
           <tbody>
              <?php
               while($ver_pers = mysqli_fetch_array($eje_pers))
               {
               ?>
               <tr>
                   <td><?php echo $ver_pers['id_personal']; ?></td>
                   <td><?php echo $ver_pers['nombre_personal']." ".$ver_pers['apellido_personal']; ?></td>
                   <td><?php echo $ver_pers['direccion_personal']; ?></td>
                   <td><?php echo $ver_pers['telefono_personal']; ?></td>
                   <td>
                   <?php
                        $FechaFormat = new datetime($ver_pers['fecha_nac_personal']);
                        $FechaNac = $FechaFormat->format('d-m-Y');
                        echo $FechaNac;
                    
                       ?>
                   </td>
                   <td><?php echo $ver_pers['sexo_personal']; ?></td>                   
                   <td><a href="editar_personal.php?id_personal=<?php echo $ver_pers['id_personal']; ?>" class="btn btn-primary btn-xs">Editar</a></td>
                   <td><a href="eliminar_personal.php?id_personal=<?php echo $ver_pers['id_personal']; ?>" class="btn btn-danger btn-xs">Eliminar</a></td>
               </tr>
               <?php
               }
               ?>
           </tbody>
       </table>
   </div>
    
</body>
</html>