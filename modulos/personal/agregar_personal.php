<?php
include_once '../../conexion/conectar.php';

if(isset($_POST['NuevoPers']))
{
    $NombrePers = $_POST['NombrePers'];
    $ApellidoPers = $_POST['ApellidoPers'];
    $TelefonoPers = $_POST['TelefonoPers'];
    $SexoPers = $_POST['SexoPers'];
    $DireccionPers = $_POST['DireccionPers'];
    $FechaNacPers = $_POST['FechaNacPers'];
    
    $FechaFormat = new datetime($FechaNacPers);
    $FechaNac = $FechaFormat->format('Y-m-d');
    
    $InsertarPers = "INSERT INTO personal VALUES (NULL,'$NombrePers','$ApellidoPers','$FechaNac','$TelefonoPers','$SexoPers','$DireccionPers')";
    
    if(mysqli_query($Cnn,$InsertarPers))
    {
        header("location: index.php");
    }
    else
    {
        echo mysqli_error();
    }
}
else
{
?>


<link rel="stylesheet" href="../../styles/css/bootstrap.min.css">

<div class="container">
    <form method="post">
        <div class="form-group col-md-12">
            <label for="NombrePers ">Nombre del Personal</label>
            <input type="text" class="form-control" name="NombrePers" placeholder="Nombre del Personal">
        </div>
        <div class="form-group col-md-12">
            <label for="ApellidoPers">Apellido del Personal</label>
            <input type="text" class="form-control" name="ApellidoPers" placeholder="Apellido del Personal">
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="FechaNacPers">Fecha Nacimiento</label>
                <input type="date" class="form-control" dateFormat="yyyy-mm-dd" name="FechaNacPers">
            </div>
            <div class="form-group col-md-4">
                <label for="TelefonoPers">Telefono</label>
                <input type="text" class="form-control" name="TelefonoPers" pattern="[0-9]{4}-[0-9]{4}" maxlength="9" placeholder="0000-0000">
            </div>
            <div class="form-group col-md-4">
                <label for="SexoPac">Sexo</label>
                <select name="SexoPers" class="form-control">
                    <option value="masculino">Masculino</option>
                    <option value="femenino" selected>Femenino</option>
                </select>
            </div>
        </div>
        <div class="form-group col-md-12">
            <label for="DireccionPers">Direccion</label>
            <textarea name="DireccionPers" class="form-control" cols="30" rows="5" style="resize: none;"></textarea>
        </div>
        <button type="submit" name="NuevoPers" class="btn btn-success">Agregar Nuevo</button>
    </form>
</div>
<?php
}
?>