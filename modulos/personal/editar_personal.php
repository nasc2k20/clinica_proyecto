<?php
include_once '../../conexion/conectar.php';

if(isset($_POST['NuevoPers']))
{
    $IdPersonal = $_GET['id_personal'];
    $NombrePers = $_POST['NombrePers'];
    $ApellidoPers = $_POST['ApellidoPers'];
    $TelefonoPers = $_POST['TelefonoPers'];
    $SexoPers = $_POST['SexoPers'];
    $DireccionPers = $_POST['DireccionPers'];
    $FechaNacPers = $_POST['FechaNacPers'];
    
    $FechaFormat = new datetime($FechaNacPac);
    $FechaNac = $FechaFormat->format('Y-m-d');
    
    $EditarPers = "UPDATE personal 
                    SET 
                    nombre_personal = '$NombrePers',
                    apellido_personal = '$ApellidoPers',
                    fecha_nac_personal = '$FechaNac',
                    telefono_personal = '$TelefonoPers',
                    sexo_personal = '$SexoPers',
                    direccion_personal = '$DireccionPers' 
                    WHERE id_personal='$IdPersonal'";
    
    if(mysqli_query($Cnn,$EditarPers))
    {
        header("location: index.php");
    }
    else
    {
        echo mysqli_error();
    }
}
else
{
    include_once '../../conexion/conectar.php';
    $IdPersonal = $_GET['id_personal'];
    
    $sel_pers = "SELECT * FROM personal WHERE id_personal=$IdPersonal";
    $eje_pers = mysqli_query($Cnn, $sel_pers);
    $ver_pers = mysqli_fetch_array($eje_pers);
?>


<link rel="stylesheet" href="../../styles/css/bootstrap.min.css">

<div class="container">
    <form method="post">
        <div class="form-group col-md-12">
            <label for="NombrePers">Nombre del Personal</label>
            <input type="text" class="form-control" name="NombrePers" placeholder="Nombre del Paciente" value="<?php echo $ver_pers['nombre_personal']; ?>">
        </div>
        <div class="form-group col-md-12">
            <label for="ApellidoPers">Apellido del Personal</label>
            <input type="text" class="form-control" name="ApellidoPers" placeholder="Apellido del Paciente" value="<?php echo $ver_pers['apellido_personal']; ?>" >
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="FechaNacPers">Fecha Nacimiento</label>
                <input type="date" class="form-control" dateFormat="yyyy-mm-dd" name="FechaNacPers" value="<?php echo $ver_pers['fecha_nac_personal']; ?>">
            </div>
            <div class="form-group col-md-4">
                <label for="TelefonoPers">Telefono</label>
                <input type="text" class="form-control" name="TelefonoPers" pattern="[0-9]{4}-[0-9]{4}" maxlength="9" placeholder="0000-0000" value="<?php echo $ver_pers['telefono_personal']; ?>">
            </div>
            <div class="form-group col-md-4">
                <label for="SexoPac">Sexo</label>
                <select name="SexoPers" class="form-control">
                    <option value="<?php echo $ver_pers['sexo_personal']; ?>"><?php echo $ver_pers['sexo_personal']; ?></option>
                    <option value="masculino">Masculino</option>
                    <option value="femenino">Femenino</option>
                </select>
            </div>
        </div>
        <div class="form-group col-md-12">
            <label for="DireccionPers">Direccion</label>
            <textarea name="DireccionPers" class="form-control" cols="30" rows="5" style="resize: none;"><?php echo $ver_pers['direccion_personal']; ?></textarea>
        </div>
        <button type="submit" name="NuevoPers" class="btn btn-primary">Actualizar</button>
    </form>
</div>
<?php
}
?>