<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../styles/css/bootstrap.min.css">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-md-6">
                <a href="rep_citas_paciente.php" class="thumbnail">
                    <h4>Reporte de Citas por Paciente</h4>
                </a>
            </div>
            
            <div class="col-xs-6 col-md-6">
                <a href="re" class="thumbnail">
                    <h4>Reporte de Paciente por Personal</h4>
                </a>
            </div>
            
            <div class="col-xs-6 col-md-6">
                <a href="rep_medicina_cita.php" class="thumbnail">
                    <h4>Reporte de Medicina por Cita</h4>
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-6 col-md-6">
                <a href="#" class="thumbnail">
                    <h4>Reporte de Citas Fecha y Hora</h4>
                </a>
            </div>
        </div>
    </div>
</body>

</html>
