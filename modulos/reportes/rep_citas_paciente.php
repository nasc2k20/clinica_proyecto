<?php
include_once '../../conexion/conectar.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../styles/css/bootstrap.min.css">
</head>

<body>
    <div class="container">
        <form action="" method="post">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="NombrePaci">Paciente</label>
                    <?php
                $sel_paci = "SELECT * FROM paciente ORDER BY nombre_paciente ASC";
                $eje_paci = mysqli_query($Cnn, $sel_paci);
                
                ?>
                    <select name="NombrePaci" class="form-control">
                        <?php
                    while($ver_paci = mysqli_fetch_array($eje_paci))
                    {
                    ?>
                        <option value="<?php echo $ver_paci['id_paciente']; ?>"><?php echo $ver_paci['nombre_paciente']." ".$ver_paci['apellido_paciente']; ?></option>
                        <?php
                    }
                    ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <button class="btn btn-success" name="GenerarReporte" type="submit">Generar Reporte</button>
                </div>
            </div>
        </form>
    </div>

    <?php
    if(isset($_POST['GenerarReporte'])){
    
    $IdPaciente =$_POST['NombrePaci'];
    $sel_cita = "SELECT * 
                FROM cita a 
                INNER JOIN paciente b ON b.id_paciente=a.id_paciente 
                INNER JOIN personal c ON c.id_personal=a.id_personal 
                WHERE a.id_paciente=$IdPaciente 
                ORDER BY a.hora_fecha_genada ASC";
    $eje_cita = mysqli_query($Cnn, $sel_cita);
    ?>

    <a href="#" class="btn btn-primary" onclick="window.print();">Imprimir</a>
    <div style="height: 4px;"></div>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th>Id</th>
                <th>Fecha y Hora Cita</th>
                <th>Asignado A</th>
                <th>Estado</th>
            </tr>
        </thead>
        <tbody>
            <?php
               while($ver_cita = mysqli_fetch_array($eje_cita))
               {
               ?>
            <tr>
                <td><?php echo $ver_cita['id_cita']; ?></td>
                <td>
                    <?php
                   $FechaFormat = new datetime($ver_cita['fecha_cita']);
                        $FechaCita = $FechaFormat->format('d-m-Y');
                        echo $FechaCita." ".$ver_cita['hora_cita'];
                    ?>
                </td>
                <td><?php echo $ver_cita['nombre_personal']." ".$ver_cita['apellido_personal']; ?></td>
                <td><?php echo $ver_cita['estado_cita']; ?></td>
            </tr>
            <?php
               }
               ?>
        </tbody>
    </table>
<?php
    }
    ?>


</body>

</html>
