<?php
include_once '../../conexion/conectar.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../styles/css/bootstrap.min.css">
</head>

<body>
    <div class="container">
        <form action="" method="post">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="NombrePaci">Cita</label>
                    <?php
                $sel_cita = "SELECT * 
                            FROM cita a 
                            INNER JOIN paciente b ON b.id_paciente=a.id_paciente 
                            ORDER BY a.hora_fecha_genada DESC";
                $eje_cita = mysqli_query($Cnn, $sel_cita);
                
                ?>
                    <select name="NombrePaci" class="form-control">
                        <?php
                    while($ver_cita = mysqli_fetch_array($eje_cita))
                    {
                    ?>
                        <option value="<?php echo $ver_cita['id_cita']; ?>"><?php echo $ver_cita['fecha_cita']." ".$ver_cita['hora_cita']." ->> ".$ver_cita['nombre_paciente']." ".$ver_cita['apellido_paciente']; ?></option>
                        <?php
                    }
                    ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <button class="btn btn-success" name="GenerarReporte" type="submit">Generar Reporte</button>
                </div>
            </div>
        </form>
    </div>

    <?php
    if(isset($_POST['GenerarReporte'])){
    
    $IdCita =$_POST['NombrePaci'];
    $sel_MedC = "SELECT * 
                FROM asignar_medicina a 
                INNER JOIN cita b ON b.id_cita=a.id_cita 
                INNER JOIN medicina c ON c.id_medicina=a.id_medicina 
                WHERE a.id_cita=$IdCita 
                ORDER BY c.nombre_medicina ASC";
    $eje_MedC = mysqli_query($Cnn, $sel_MedC);
    ?>

    <a href="#" class="btn btn-primary" onclick="window.print();">Imprimir</a>
    <div style="height: 4px;"></div>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre Medicina</th>
                <th>Dosis</th>
            </tr>
        </thead>
        <tbody>
            <?php
               while($ver_MedC = mysqli_fetch_array($eje_MedC))
               {
               ?>
            <tr>
                <td><?php echo $ver_MedC['id_asignar_medicina']; ?></td>
                <td><?php echo $ver_MedC['nombre_medicina']; ?></td>
                <td><?php echo $ver_MedC['dosis_medicina']; ?></td>
            </tr>
            <?php
               }
               ?>
        </tbody>
    </table>
<?php
    }
    ?>


</body>

</html>
