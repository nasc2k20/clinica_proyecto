<?php
include_once '../../conexion/conectar.php';

date_default_timezone_set("America/El_Salvador");

if(isset($_POST['NuevaCita']))
{
    $FechaCitaT = $_POST['FechaCita'];
    $HoraCitaT = $_POST['HoraCita'];
    $FechaGenera = date('Y-m-d H:i:s');
    $IdPaciente = $_POST['NombrePaci'];
    $IdPersonal = $_POST['NombrePersonal'];
    $EstadoCita = "pendiente";
    
    $FechaFormat = new datetime($FechaCitaT);
    $FechaCita = $FechaFormat->format('Y-m-d');
    $HoraFormat = new datetime($HoraCitaT);
    $HoraCita = $HoraFormat->format('H:i');
    
    $InsertarCita = "INSERT INTO cita VALUES (NULL,'$FechaGenera','$FechaCita','$HoraCita',
                    '$IdPaciente','$IdPersonal','$EstadoCita')";
    
    if(mysqli_query($Cnn,$InsertarCita))
    {
        header("location: index.php");
    }
    else
    {
        echo mysqli_error();
    }
}
else
{
?>


<link rel="stylesheet" href="../../styles/css/bootstrap.min.css">

<div class="container">
    <form method="post">
       <div class="row">
            <div class="form-group col-md-6">
                <label for="FechaCita">Fecha</label>
                <input type="date" class="form-control" name="FechaCita" placeholder="yyyy-mm-dd" required>
            </div>
            <div class="form-group col-md-6">
                <label for="HoraCita">Hora</label>
                <input type="time" class="form-control" placeholder="00:00" name="HoraCita" required>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="NombrePaci">Paciente</label>
                <?php
                $sel_paci = "SELECT * FROM paciente ORDER BY nombre_paciente ASC";
                $eje_paci = mysqli_query($Cnn, $sel_paci);
                
                ?>
                <select name="NombrePaci" class="form-control">
                   <?php
                    while($ver_paci = mysqli_fetch_array($eje_paci))
                    {
                    ?>
                    <option value="<?php echo $ver_paci['id_paciente']; ?>"><?php echo $ver_paci['nombre_paciente']." ".$ver_paci['apellido_paciente']; ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="NombrePersonal">Personal</label>
                <?php
                $sel_pers = "SELECT * FROM personal ORDER BY nombre_personal ASC";
                $eje_pers = mysqli_query($Cnn, $sel_pers);
                
                ?>
                <select name="NombrePersonal" class="form-control">
                   <?php
                    while($ver_pers = mysqli_fetch_array($eje_pers))
                    {
                    ?>
                    <option value="<?php echo $ver_pers['id_personal']; ?>"><?php echo $ver_pers['nombre_personal']." ".$ver_pers['apellido_personal']; ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <button type="submit" name="NuevaCita" class="btn btn-success">Agregar Nuevo</button>
    </form>
</div>
<?php
}
?>
