<?php
include_once '../../conexion/conectar.php';

$sel_cita = "SELECT * 
                FROM cita a 
                INNER JOIN paciente b ON b.id_paciente=a.id_paciente 
                INNER JOIN personal c ON c.id_personal=a.id_personal 
                ORDER BY a.hora_fecha_genada ASC";
$eje_cita = mysqli_query($Cnn, $sel_cita);
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../styles/css/bootstrap.min.css">
</head>

<body>
    <div class="container">
        <span class="pull-rigth">
            <a href="agregar_cita.php" class="btn btn-success btn-xs">Nuevo</a>
        </span>
        <div style="height: 4px;"></div>
        <table class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Fecha y Hora Cita</th>
                    <th>Nombre Paciente</th>
                    <th>Asignado A</th>
                    <th>Estado</th>
                    <th colspan="4">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
               while($ver_cita = mysqli_fetch_array($eje_cita))
               {
               ?>
                <tr>
                    <td><?php echo $ver_cita['id_cita']; ?></td>
                    <td>
                        <?php
                   $FechaFormat = new datetime($ver_cita['fecha_cita']);
                        $FechaCita = $FechaFormat->format('d-m-Y');
                        echo $FechaCita." ".$ver_cita['hora_cita'];
                    ?>
                    </td>
                    <td><?php echo $ver_cita['nombre_paciente']." ".$ver_cita['apellido_paciente']; ?></td>
                    <td><?php echo $ver_cita['nombre_personal']." ".$ver_cita['apellido_personal']; ?></td>
                    <td><?php echo $ver_cita['estado_cita']; ?></td>
                    <td>
                        <?php if ($ver_cita['estado_cita']=='finalizada') {?>
                        <a class="btn btn-warning btn-xs" disabled>Finalizar Cita</a>
                        <?php }else{ ?>
                        <a href="aprobar_cita.php?id_cita=<?php echo $ver_cita['id_cita']; ?>" class="btn btn-warning btn-xs" <?php echo ($ver_cita['estado_cita']=='finalizada')? 'disabled' : '';?>>Finalizar Cita</a>
                        <?php }?>
                    </td>
                    <td>
                        <?php if ($ver_cita['estado_cita']=='finalizada') {?>
                        <a class="btn btn-default btn-xs" disabled>Asignar Medicina</a>
                        <?php }else{ ?>
                        <a href="../asignar_medicina/index.php?id_cita=<?php echo $ver_cita['id_cita']; ?>&id_paciente=<?php echo $ver_cita['id_paciente']; ?>" class="btn btn-default btn-xs">Asignar Medicina</a>
                        <?php }?>
                    </td>
                    <td><a href="editar_cita.php?id_cita=<?php echo $ver_cita['id_cita']; ?>" class="btn btn-primary btn-xs">Editar</a></td>
                    <td><a href="eliminar_cita.php?id_cita=<?php echo $ver_cita['id_cita']; ?>" class="btn btn-danger btn-xs">Eliminar</a></td>
                </tr>
                <?php
               }
               ?>
            </tbody>
        </table>
    </div>

</body>

</html>
