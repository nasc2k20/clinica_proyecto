<?php
include_once '../../conexion/conectar.php';

if(isset($_POST['NuevoPac']))
{
    $IdPaciente = $_GET['id_paciente'];
    $NombrePac = $_POST['NombrePac'];
    $ApellidoPac = $_POST['ApellidoPac'];
    $TelefonoPac = $_POST['TelefonoPac'];
    $SexoPac = $_POST['SexoPac'];
    $DireccionPac = $_POST['DireccionPac'];
    $FechaNacPac = $_POST['FechaNacPac'];
    
    $FechaFormat = new datetime($FechaNacPac);
    $FechaNac = $FechaFormat->format('Y-m-d');
    
    $EditarPac = "UPDATE paciente 
                    SET 
                    nombre_paciente = '$NombrePac',
                    apellido_paciente = '$ApellidoPac',
                    fecha_nac_paciente = '$FechaNac',
                    telefono_paciente = '$TelefonoPac',
                    sexo_paciente = '$SexoPac',
                    direccion_paciente = '$DireccionPac' 
                    WHERE id_paciente='$IdPaciente'";
    
    if(mysqli_query($Cnn,$EditarPac))
    {
        header("location: index.php");
    }
    else
    {
        echo mysqli_error();
    }
}
else
{
    include_once '../../conexion/conectar.php';
    $IdPaciente = $_GET['id_paciente'];
    
    $sel_paci = "SELECT * FROM paciente WHERE id_paciente=$IdPaciente";
    $eje_paci = mysqli_query($Cnn, $sel_paci);
    $ver_paci = mysqli_fetch_array($eje_paci);
?>


<link rel="stylesheet" href="../../styles/css/bootstrap.min.css">

<div class="container">
    <form method="post">
        <div class="form-group col-md-12">
            <label for="NombrePac ">Nombre del Paciente</label>
            <input type="text" class="form-control" name="NombrePac" placeholder="Nombre del Paciente" value="<?php echo $ver_paci['nombre_paciente']; ?>">
        </div>
        <div class="form-group col-md-12">
            <label for="ApellidoPac">Apellido del Paciente</label>
            <input type="text" class="form-control" name="ApellidoPac" placeholder="Apellido del Paciente" value="<?php echo $ver_paci['apellido_paciente']; ?>" >
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="FechaNacPac">Fecha Nacimiento</label>
                <input type="date" class="form-control" dateFormat="yyyy-mm-dd" name="FechaNacPac" value="<?php echo $ver_paci['fecha_nac_paciente']; ?>">
            </div>
            <div class="form-group col-md-4">
                <label for="TelefonoPac">Telefono</label>
                <input type="text" class="form-control" name="TelefonoPac" pattern="[0-9]{4}-[0-9]{4}" maxlength="9" placeholder="0000-0000" value="<?php echo $ver_paci['telefono_paciente']; ?>">
            </div>
            <div class="form-group col-md-4">
                <label for="SexoPac">Sexo</label>
                <select name="SexoPac" class="form-control">
                    <option value="<?php echo $ver_paci['sexo_paciente']; ?>"><?php echo $ver_paci['sexo_paciente']; ?></option>
                    <option value="masculino">Masculino</option>
                    <option value="femenino">Femenino</option>
                </select>
            </div>
        </div>
        <div class="form-group col-md-12">
            <label for="DireccionPac">Direccion</label>
            <textarea name="DireccionPac" class="form-control" cols="30" rows="5" style="resize: none;"><?php echo $ver_paci['direccion_paciente']; ?></textarea>
        </div>
        <button type="submit" name="NuevoPac" class="btn btn-primary">Actualizar</button>
    </form>
</div>
<?php
}
?>