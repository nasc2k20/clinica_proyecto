<?php
include_once '../../conexion/conectar.php';

if(isset($_POST['NuevoPac']))
{
    $NombrePac = $_POST['NombrePac'];
    $ApellidoPac = $_POST['ApellidoPac'];
    $TelefonoPac = $_POST['TelefonoPac'];
    $SexoPac = $_POST['SexoPac'];
    $DireccionPac = $_POST['DireccionPac'];
    $FechaNacPac = $_POST['FechaNacPac'];
    
    $FechaFormat = new datetime($FechaNacPac);
    $FechaNac = $FechaFormat->format('Y-m-d');
    
    $InsertarPac = "INSERT INTO paciente VALUES (NULL,'$NombrePac','$ApellidoPac','$FechaNac','$TelefonoPac','$SexoPac','$DireccionPac')";
    
    if(mysqli_query($Cnn,$InsertarPac))
    {
        header("location: index.php");
    }
    else
    {
        echo mysqli_error();
    }
}
else
{
?>


<link rel="stylesheet" href="../../styles/css/bootstrap.min.css">

<div class="container">
    <form method="post">
        <div class="form-group col-md-12">
            <label for="NombrePac ">Nombre del Paciente</label>
            <input type="text" class="form-control" name="NombrePac" placeholder="Nombre del Paciente">
        </div>
        <div class="form-group col-md-12">
            <label for="ApellidoPac">Apellido del Paciente</label>
            <input type="text" class="form-control" name="ApellidoPac" placeholder="Apellido del Paciente">
        </div>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="FechaNacPac">Fecha Nacimiento</label>
                <input type="date" class="form-control" dateFormat="yyyy-mm-dd" name="FechaNacPac">
            </div>
            <div class="form-group col-md-4">
                <label for="TelefonoPac">Telefono</label>
                <input type="text" class="form-control" name="TelefonoPac" pattern="[0-9]{4}-[0-9]{4}" maxlength="9" placeholder="0000-0000">
            </div>
            <div class="form-group col-md-4">
                <label for="SexoPac">Sexo</label>
                <select name="SexoPac" class="form-control">
                    <option value="masculino">Masculino</option>
                    <option value="femenino" selected>Femenino</option>
                </select>
            </div>
        </div>
        <div class="form-group col-md-12">
            <label for="DireccionPac">Direccion</label>
            <textarea name="DireccionPac" class="form-control" cols="30" rows="5" style="resize: none;"></textarea>
        </div>
        <button type="submit" name="NuevoPac" class="btn btn-success">Agregar Nuevo</button>
    </form>
</div>
<?php
}
?>