<?php
include_once '../../conexion/conectar.php';

$sel_paci = "SELECT * FROM paciente ORDER BY nombre_paciente ASC";
$eje_paci = mysqli_query($Cnn, $sel_paci);


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../styles/css/bootstrap.min.css">
</head>
<body>
   <div class="container">
      <span class="pull-rigth">
          <a href="agregar_paciente.php" class="btn btn-success btn-xs">Nuevo</a>
      </span>
      <div style="height: 4px;"></div>
       <table class="table table-hover table-bordered">
           <thead>
               <tr>
                   <th>Id</th>
                   <th>Nombre Completo</th>
                   <th>Direccion</th>
                   <th>Telefono</th>
                   <th>Fecha Nac.</th>
                   <th>Sexo</th>
                   <th colspan="2">Acciones</th>
               </tr>
           </thead>
           <tbody>
              <?php
               while($ver_paci = mysqli_fetch_array($eje_paci))
               {
               ?>
               <tr>
                   <td><?php echo $ver_paci['id_paciente']; ?></td>
                   <td><?php echo $ver_paci['nombre_paciente']." ".$ver_paci['apellido_paciente']; ?></td>
                   <td><?php echo $ver_paci['direccion_paciente']; ?></td>
                   <td><?php echo $ver_paci['telefono_paciente']; ?></td>
                   <td>
                   <?php
                        $FechaFormat = new datetime($ver_paci['fecha_nac_paciente']);
                        $FechaNac = $FechaFormat->format('d-m-Y');
                        echo $FechaNac;
                    
                       ?>
                   </td>
                   <td><?php echo $ver_paci['sexo_paciente']; ?></td>                   
                   <td><a href="editar_paciente.php?id_paciente=<?php echo $ver_paci['id_paciente']; ?>" class="btn btn-primary btn-xs">Editar</a></td>
                   <td><a href="eliminar_paciente.php?id_paciente=<?php echo $ver_paci['id_paciente']; ?>" class="btn btn-danger btn-xs">Eliminar</a></td>
               </tr>
               <?php
               }
               ?>
           </tbody>
       </table>
   </div>
    
</body>
</html>