<?php
include_once '../../conexion/conectar.php';

if(isset($_POST['NuevaMed']))
{
    $IdMedicina = $_GET['id_medicina'];
    $NombreMedicina = $_POST['NombreMedicina'];
    $NombreFab = $_POST['NombreFab'];
    $FechaVence = $_POST['FechaVence'];
    
    $FechaFormat = new datetime($FechaVence);
    $FechaNac = $FechaFormat->format('Y-m-d');
    
    $EditarMedi = "UPDATE medicina  
                    SET 
                    nombre_medicina = '$NombreMedicina',
                    nombre_fabricante = '$NombreFab',
                    fecha_vence = '$FechaNac'  
                    WHERE id_medicina='$IdMedicina'";
    
    if(mysqli_query($Cnn,$EditarMedi))
    {
        header("location: index.php");
    }
    else
    {
        echo mysqli_error();
    }
}
else
{
    include_once '../../conexion/conectar.php';
    $IdMedicina = $_GET['id_medicina'];
    
    $sel_medicina = "SELECT * FROM medicina WHERE id_medicina=$IdMedicina";
    $eje_medicina = mysqli_query($Cnn, $sel_medicina);
    $ver_medicina = mysqli_fetch_array($eje_medicina);
?>


<link rel="stylesheet" href="../../styles/css/bootstrap.min.css">

<div class="container">
    <form method="post">
        <div class="form-group col-md-12">
            <label for="NombreMedicina">Nombre de la Medicina</label>
            <input type="text" class="form-control" name="NombreMedicina" placeholder="Nombre del Paciente" value="<?php echo $ver_medicina['nombre_medicina']; ?>">
        </div>
        <div class="row">
           <div class="form-group col-md-6">
                <label for="NombreFab">Fabricante</label>
                <input type="text" class="form-control" name="NombreFab" value="<?php echo $ver_medicina['nombre_fabricante']; ?>">
            </div>
            <div class="form-group col-md-6">
                <label for="FechaVence">Fecha Vencimiento</label>
                <input type="date" class="form-control" dateFormat="yyyy-mm-dd" name="FechaVence" value="<?php echo $ver_medicina['fecha_vence']; ?>">
            </div>
        </div>
        <button type="submit" name="NuevaMed" class="btn btn-primary">Actualizar</button>
    </form>
</div>
<?php
}
?>