<?php
include_once '../../conexion/conectar.php';

if(isset($_POST['NuevaMed']))
{
    $NombreMedicina = $_POST['NombreMedicina'];
    $NombreFab = $_POST['NombreFab'];
    $FechaVence = $_POST['FechaVence'];
    
    $FechaFormat = new datetime($FechaVence);
    $FechaNac = $FechaFormat->format('Y-m-d');
    
    $InsertarMedi = "INSERT INTO medicina VALUES (NULL,'$NombreMedicina','$NombreFab','$FechaNac')";
    
    if(mysqli_query($Cnn,$InsertarMedi))
    {
        header("location: index.php");
    }
    else
    {
        echo mysqli_error();
    }
}
else
{
?>


<link rel="stylesheet" href="../../styles/css/bootstrap.min.css">

<div class="container">
    <form method="post">
        <div class="form-group col-md-12">
            <label for="NombreMedicina">Nombre de la Medicina</label>
            <input type="text" class="form-control" name="NombreMedicina" placeholder="Nombre de la Medicina">
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="NombreFab">Fabricante</label>
                <input type="text" class="form-control" name="NombreFab" placeholder="Nombre del Fabricante">
            </div>
            <div class="form-group col-md-6">
                <label for="FechaVence">Fecha Vencimiento</label>
                <input type="date" class="form-control" dateFormat="yyyy-mm-dd" name="FechaVence">
            </div>
        </div>
        <button type="submit" name="NuevaMed" class="btn btn-success">Agregar Nuevo</button>
    </form>
</div>
<?php
}
?>
