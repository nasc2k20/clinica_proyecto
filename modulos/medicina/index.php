<?php
include_once '../../conexion/conectar.php';

$sel_medicina = "SELECT * FROM medicina ORDER BY nombre_medicina ASC";
$eje_medicina = mysqli_query($Cnn, $sel_medicina);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../styles/css/bootstrap.min.css">
</head>
<body>
   <div class="container">
      <span class="pull-rigth">
          <a href="agregar_medicina.php" class="btn btn-success btn-xs">Nuevo</a>
      </span>
      <div style="height: 4px;"></div>
       <table class="table table-hover table-bordered">
           <thead>
               <tr>
                   <th>Id</th>
                   <th>Nombre Medicina</th>
                   <th>Fabricante</th>
                   <th>Vencimiento</th>
                   <th colspan="2">Acciones</th>
               </tr>
           </thead>
           <tbody>
              <?php
               while($ver_medicina = mysqli_fetch_array($eje_medicina))
               {
               ?>
               <tr>
                   <td><?php echo $ver_medicina['id_medicina']; ?></td>
                   <td><?php echo $ver_medicina['nombre_medicina']; ?></td>
                   <td><?php echo $ver_medicina['nombre_fabricante']; ?></td>
                   <td>
                   <?php
                        $FechaFormat = new datetime($ver_medicina['fecha_vence']);
                        $FechaNac = $FechaFormat->format('d-m-Y');
                        echo $FechaNac;
                    
                       ?>
                   </td>
                   <td><a href="editar_medicina.php?id_medicina=<?php echo $ver_medicina['id_medicina']; ?>" class="btn btn-primary btn-xs">Editar</a></td>
                   <td><a href="eliminar_medicina.php?id_medicina=<?php echo $ver_medicina['id_medicina']; ?>" class="btn btn-danger btn-xs">Eliminar</a></td>
               </tr>
               <?php
               }
               ?>
           </tbody>
       </table>
   </div>
    
</body>
</html>