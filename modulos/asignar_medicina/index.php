<?php
include_once '../../conexion/conectar.php';
$IdCita = $_GET['id_cita'];
$IdPaciente = $_GET['id_paciente'];

$sel_asignacion = "SELECT * 
                FROM asignar_medicina a 
                INNER JOIN cita b ON b.id_cita=a.id_cita 
                INNER JOIN medicina c ON c.id_medicina=a.id_medicina 
                WHERE a.id_paciente=$IdPaciente AND a.id_cita=$IdCita 
                ORDER BY c.nombre_medicina ASC";
$eje_asignacion = mysqli_query($Cnn, $sel_asignacion);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../styles/css/bootstrap.min.css">
</head>
<body>
   <div class="container">
      <span class="pull-rigth">
          <a href="agregar_asignarMed.php?id_cita=<?php echo $IdCita; ?>&id_paciente=<?php echo $IdPaciente; ?>" class="btn btn-success btn-xs">Nuevo</a>
      </span>
      <span class="pull-rigth">
          <a href="../citas/index.php" class="btn btn-primary btn-xs">&laquo;&laquo;Volver</a>
      </span>
      <div style="height: 4px;"></div>
       <table class="table table-hover table-bordered">
           <thead>
               <tr>
                   <th>Id</th>
                   <th>Nombre Medicina</th>
                   <th>Dosis</th>
                   <th>Accion</th>
               </tr>
           </thead>
           <tbody>
              <?php
               while($ver_asignacion = mysqli_fetch_array($eje_asignacion))
               {
               ?>
               <tr>
                   <td><?php echo $ver_asignacion['id_asignar_medicina']; ?></td>
                   <td><?php echo $ver_asignacion['nombre_medicina']; ?></td>
                   <td><?php echo $ver_asignacion['dosis_medicina']; ?></td>
                   <td><a href="eliminar_asignarMed.php?id_asignar=<?php echo $ver_asignacion['id_asignar_medicina']; ?>&id_cita=<?php echo $ver_asignacion['id_cita']?>&id_paciente=<?php echo $ver_asignacion['id_paciente']?>" class="btn btn-danger btn-xs">Eliminar</a></td>
               </tr>
               <?php
               }
               ?>
           </tbody>
       </table>
   </div>
    
</body>
</html>