<?php
include_once '../../conexion/conectar.php';
$IdCita = $_GET['id_cita'];
$IdPaciente = $_GET['id_paciente'];

if(isset($_POST['NuevaAsigMed']))
{
    $NombreMedicina = $_POST['NombreMedicina'];
    $DosisMedicina = $_POST['DosisMedicina'];
    
    $InsertarAsigM = "INSERT INTO asignar_medicina VALUES (NULL,'$IdPaciente','$IdCita','$NombreMedicina','$DosisMedicina')";
    
    if(mysqli_query($Cnn,$InsertarAsigM))
    {
        header("location: index.php?id_cita=$IdCita&id_paciente=$IdPaciente");
    }
    else
    {
        echo mysqli_error();
    }
}
else
{
?>


<link rel="stylesheet" href="../../styles/css/bootstrap.min.css">

<div class="container">
    <form method="post">
        <div class="row">
            <div class="form-group col-md-12">
                <label for="NombreMedicina">Medicina</label>
                <?php
                $sel_medicina = "SELECT * FROM medicina ORDER BY nombre_medicina ASC";
                $eje_medicina = mysqli_query($Cnn, $sel_medicina);
                
                ?>
                <select name="NombreMedicina" class="form-control">
                    <?php
                    while($ver_medicina = mysqli_fetch_array($eje_medicina))
                    {
                    ?>
                    <option value="<?php echo $ver_medicina['id_medicina']; ?>"><?php echo $ver_medicina['nombre_medicina']; ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="DosisMedicina">Dosis</label>
                    <input type="text" class="form-control" name="DosisMedicina" placeholder="Dosis de la Medicina" required>
                </div>
            </div>
        </div>
        <button type="submit" name="NuevaAsigMed" class="btn btn-success">Agregar Nuevo</button>
    </form>
</div>
<?php
}
?>
