/*
 Navicat Premium Data Transfer

 Source Server         : LOCALHOST
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : clinica_sys

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 21/06/2019 09:57:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for asignar_medicina
-- ----------------------------
DROP TABLE IF EXISTS `asignar_medicina`;
CREATE TABLE `asignar_medicina`  (
  `id_asignar_medicina` int(15) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_paciente` int(15) NOT NULL,
  `id_cita` int(15) NOT NULL,
  `id_medicina` int(15) NOT NULL,
  `dosis_medicina` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id_asignar_medicina`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of asignar_medicina
-- ----------------------------
INSERT INTO `asignar_medicina` VALUES (1, 3, 1, 4, 'tomar 1 al dia');
INSERT INTO `asignar_medicina` VALUES (2, 3, 1, 1, 'tomar 2 tres veces al dia');
INSERT INTO `asignar_medicina` VALUES (8, 5, 2, 3, 'tomar en una sola dosis');
INSERT INTO `asignar_medicina` VALUES (9, 4, 7, 1, 'tomar 2 tres veces al dia');
INSERT INTO `asignar_medicina` VALUES (10, 4, 7, 2, 'tomar 1 tres veces al dia');

-- ----------------------------
-- Table structure for cita
-- ----------------------------
DROP TABLE IF EXISTS `cita`;
CREATE TABLE `cita`  (
  `id_cita` int(15) UNSIGNED NOT NULL AUTO_INCREMENT,
  `hora_fecha_genada` datetime(0) NOT NULL,
  `fecha_cita` date NOT NULL,
  `hora_cita` time(0) NOT NULL,
  `id_paciente` int(15) NOT NULL,
  `id_personal` int(15) NOT NULL,
  `estado_cita` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id_cita`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cita
-- ----------------------------
INSERT INTO `cita` VALUES (1, '2019-06-21 03:01:29', '2019-10-20', '14:25:00', 3, 3, 'finalizada');
INSERT INTO `cita` VALUES (2, '2019-06-21 03:05:46', '2019-07-14', '07:08:00', 5, 2, 'pendiente');
INSERT INTO `cita` VALUES (3, '2019-06-21 03:06:09', '2019-07-04', '11:44:00', 4, 4, 'pendiente');
INSERT INTO `cita` VALUES (4, '2019-06-21 03:06:50', '2019-11-30', '08:43:00', 3, 2, 'pendiente');
INSERT INTO `cita` VALUES (5, '2019-06-21 03:07:06', '2019-10-06', '15:34:00', 3, 4, 'pendiente');
INSERT INTO `cita` VALUES (6, '2019-06-21 03:07:33', '2020-05-01', '07:43:00', 5, 3, 'pendiente');
INSERT INTO `cita` VALUES (7, '2019-06-21 03:07:54', '2020-08-12', '12:45:00', 4, 3, 'pendiente');
INSERT INTO `cita` VALUES (8, '2019-06-21 03:08:38', '2019-09-16', '10:04:00', 5, 4, 'pendiente');
INSERT INTO `cita` VALUES (9, '2019-06-21 03:09:27', '2019-10-29', '09:25:00', 4, 2, 'pendiente');

-- ----------------------------
-- Table structure for medicina
-- ----------------------------
DROP TABLE IF EXISTS `medicina`;
CREATE TABLE `medicina`  (
  `id_medicina` int(15) NOT NULL AUTO_INCREMENT,
  `nombre_medicina` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `nombre_fabricante` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `fecha_vence` date NOT NULL,
  PRIMARY KEY (`id_medicina`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of medicina
-- ----------------------------
INSERT INTO `medicina` VALUES (1, 'ACETAMINOFEN 500MG', 'SAIMED LAB', '2020-01-01');
INSERT INTO `medicina` VALUES (2, 'NAPROXENO SODICO 550MG', 'SAIMED LAB', '2020-05-01');
INSERT INTO `medicina` VALUES (3, 'ALBENDAZOL 400MG', 'SAIMED LAB', '2021-04-01');
INSERT INTO `medicina` VALUES (4, 'LOSARTAN POTASICO 50MG', 'SAIMED LAB', '2019-12-01');

-- ----------------------------
-- Table structure for paciente
-- ----------------------------
DROP TABLE IF EXISTS `paciente`;
CREATE TABLE `paciente`  (
  `id_paciente` int(10) NOT NULL AUTO_INCREMENT,
  `nombre_paciente` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `apellido_paciente` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `fecha_nac_paciente` date NOT NULL,
  `telefono_paciente` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `sexo_paciente` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `direccion_paciente` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id_paciente`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of paciente
-- ----------------------------
INSERT INTO `paciente` VALUES (3, 'Alexis', 'Marquina', '2001-01-01', '3094-2243', 'masculino', 'San Miguel');
INSERT INTO `paciente` VALUES (4, 'Mirian', 'Machado', '1990-01-01', '0232-2423', 'femenino', 'San Miguel');
INSERT INTO `paciente` VALUES (5, 'Gabriel', 'Pedroza', '1990-01-01', '4432-2432', 'masculino', 'Sonsonate');

-- ----------------------------
-- Table structure for personal
-- ----------------------------
DROP TABLE IF EXISTS `personal`;
CREATE TABLE `personal`  (
  `id_personal` int(10) NOT NULL AUTO_INCREMENT,
  `nombre_personal` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `apellido_personal` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `fecha_nac_personal` date NOT NULL,
  `telefono_personal` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `sexo_personal` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `direccion_personal` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id_personal`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal
-- ----------------------------
INSERT INTO `personal` VALUES (2, 'Mirna', 'Gonzalez', '2000-01-01', '0243-2342', 'femenino', 'San Salvador');
INSERT INTO `personal` VALUES (3, 'Javier', 'Machado', '1079-01-01', '2343-2342', 'masculino', 'Santa Ana');
INSERT INTO `personal` VALUES (4, 'Procoro', 'Azul', '2019-06-21', '0234-2342', 'femenino', 'Santa Ana');

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios`  (
  `login_usuario` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login_password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nombre_completo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`login_usuario`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('admin', 'admin', 'JACQUELIN MADELIN REYES BENITEZ');

SET FOREIGN_KEY_CHECKS = 1;
